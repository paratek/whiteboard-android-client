package com.whiteboard.attendance.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AuthenticationService extends Service{

	@Override
	public IBinder onBind(Intent arg0) {
		CustomAuthenticator authenticator = new CustomAuthenticator(this);
        return authenticator.getIBinder();
	}

}
