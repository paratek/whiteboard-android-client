package com.whiteboard.attendance.util;

import java.io.Serializable;

public class Student implements Serializable {
	public String id;
	public String roll_no;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoll_no() {
		return roll_no;
	}
	public void setRoll_no(String roll_no) {
		this.roll_no = roll_no;
	}
	
	
}
