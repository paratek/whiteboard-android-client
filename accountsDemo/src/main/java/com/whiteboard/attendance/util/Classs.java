package com.whiteboard.attendance.util;

import java.util.ArrayList;

public class Classs {
	String name = "";
	String department = "";
	Integer classCapacity;
	ArrayList<Subject> subjects = new ArrayList<Subject>();
	ArrayList<Student> rollNoList = new ArrayList<Student>();
	String id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Integer getClassCapacity() {
		return classCapacity;
	}

	public void setClassCapacity(Integer classCapacity) {
		this.classCapacity = classCapacity;
	}

	public ArrayList<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(ArrayList<Subject> subjects) {
		this.subjects = subjects;
	}

	public ArrayList<Student> getRollNoList() {
		return rollNoList;
	}

	public void setRollNoList(ArrayList<Student> rollNoList) {
		this.rollNoList = rollNoList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
