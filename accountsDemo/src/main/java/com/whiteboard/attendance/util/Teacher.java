package com.whiteboard.attendance.util;

import java.util.ArrayList;

public class Teacher {
	String name="";
	String department="";
	ArrayList<Classs> classes=new ArrayList<Classs>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public ArrayList<Classs> getClasses() {
		return classes;
	}
	public void setClasses(ArrayList<Classs> classes) {
		this.classes = classes;
	}
	
	
}
