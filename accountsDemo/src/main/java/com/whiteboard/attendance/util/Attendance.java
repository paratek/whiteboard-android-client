package com.whiteboard.attendance.util;

public class Attendance {
	String className;
	String subjectName;
	String topicName;
	String date;
	TimeSlot slot;
	String type;
	Integer attendance_id;
	String timestamp;
	Integer subject_id;
	Integer module_id;
	Integer class_id;
	public Attendance(Integer attendance_id,String className, String subjectName, String topicName,
			String date, TimeSlot slot, String type, 
			String timestamp, Integer subject_id, Integer module_id,Integer class_id) {
		super();
		this.className = className;
		this.subjectName = subjectName;
		this.topicName = topicName;
		this.date = date;
		this.slot = slot;
		this.type = type;
		this.attendance_id = attendance_id;
		this.timestamp = timestamp;
		this.subject_id = subject_id;
		this.module_id = module_id;
		this.class_id=class_id;
	}
	
	public Integer getClass_id() {
		return class_id;
	}

	public void setClass_id(Integer class_id) {
		this.class_id = class_id;
	}

	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public TimeSlot getSlot() {
		return slot;
	}
	public void setSlot(TimeSlot slot) {
		this.slot = slot;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getAttendance_id() {
		return attendance_id;
	}
	public void setAttendance_id(Integer attendance_id) {
		this.attendance_id = attendance_id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public Integer getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(Integer subject_id) {
		this.subject_id = subject_id;
	}
	public Integer getModule_id() {
		return module_id;
	}
	public void setModule_id(Integer module_id) {
		this.module_id = module_id;
	}
	
	
	
}
