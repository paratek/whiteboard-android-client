package com.whiteboard.attendance.util;

import java.util.ArrayList;

public class Subject {
	String name="";
	ArrayList<Topic> topics=new ArrayList<Topic>();
	Boolean isElective=false;
	ArrayList<Student> rollNoList=new ArrayList<Student>();
	String id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Topic> getTopics() {
		return topics;
	}
	public void setTopics(ArrayList<Topic> topics) {
		this.topics = topics;
	}
	public Boolean getIsElective() {
		return isElective;
	}
	public void setIsElective(Boolean isElective) {
		this.isElective = isElective;
	}
	public ArrayList<Student> getRollNoList() {
		return rollNoList;
	}
	public void setRollNoList(ArrayList<Student> rollNoList) {
		this.rollNoList = rollNoList;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
