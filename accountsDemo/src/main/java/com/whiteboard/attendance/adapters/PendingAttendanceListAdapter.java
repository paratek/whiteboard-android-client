package com.whiteboard.attendance.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.accountsdemo.R;
import com.whiteboard.attendance.helper.DataHelper;
import com.whiteboard.attendance.helper.MessageHelper;
import com.whiteboard.attendance.util.Attendance;
import com.whiteboard.attendance.util.JsonValues;
import com.whiteboard.attendance.util.URLs;
import com.whiteboard.attendance.util.Values;

public class PendingAttendanceListAdapter extends BaseAdapter {
	Context context;
	ArrayList<Attendance> list;
	DataHelper helper;
	SQLiteDatabase db;
	Cursor cur;
	public PendingAttendanceListAdapter(Context context, ArrayList<Attendance> list) {
		super();
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	public View getConvertView(View convertView, String file,
			ArrayList<View> displayedViews) {
		if (convertView != null
				&& displayedViews.contains((ImageView) convertView))// &&!selectedFiles.contains(file))
			convertView = null;
		return convertView;
	}
	
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = convertView;
		Holder holder = null;
		LayoutInflater inflater;
		TextView className;
		TextView dateSlot;
		TextView subjectTopic;
		TextView attendanceType;
		Button push;
		final Attendance attendance;
		attendance=list.get(position);
		if (row == null) {
			inflater = (LayoutInflater) ((Activity) context)
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.pending_list_item, parent,false);
			holder = new Holder();
			className = (TextView) row.findViewById(R.id.className);
			dateSlot=(TextView) row.findViewById(R.id.dateSlot);
			subjectTopic=(TextView) row.findViewById(R.id.subjectTopic);
			attendanceType=(TextView) row.findViewById(R.id.attendanceType);
			push = (Button) row.findViewById(R.id.pushPendingAttendance);
			className.setText(attendance.getClassName());
			dateSlot.setText(attendance.getDate()+" -> "+attendance.getSlot().getStart_time()+"-"+attendance.getSlot().getEnd_time());
			subjectTopic.setText(attendance.getSubjectName()+"-"+attendance.getTopicName());
			attendanceType.setText(attendance.getType());
			push.setBackgroundResource(R.drawable.push);
			holder.className=className;
			holder.dateSlot=dateSlot;
			holder.subjectTopic=subjectTopic;
			holder.attendanceType=attendanceType;
			holder.push=push;
			row.setTag(holder);
		} else {
			holder = (Holder) row.getTag();
		}
		holder.push.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ConnectivityManager cm = (ConnectivityManager) context
						.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo netInfo = cm.getActiveNetworkInfo();
				if (netInfo != null && netInfo.isConnectedOrConnecting()) {//checking if connectivity is available
					AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
					builder2.setTitle("Confirm Attendance");
					builder2.setMessage("Do you want to submit " + " this attendance?");
					builder2.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
					builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							helper=new DataHelper(context);
							db=helper.getReadableDatabase();
							Integer attendance_id=attendance.getAttendance_id();
							cur=db.rawQuery("Select * from attendance where pendingattendance_id="+attendance_id+";", null);
							final SharedPreferences sharedpreferences = context.getSharedPreferences(Values.USER_INFO,
									0);
							new Thread(new Runnable(){
								@Override
								public void run() {
									// TODO Auto-generated method stub
									if (cur != null) {
										String teacher_name=sharedpreferences.getString(Values.TEACHER_NAME, "");
										Map<String, String> postParam = new HashMap<String, String>();
										String access_token=sharedpreferences.getString(Values.ACCESS_TOKEN, "");
										//details of JSON to be sent
										// postParam.put("title", "Faculty Login");
										//postParam.put("grant_type", "password");
										postParam.put(JsonValues.access_token,access_token);
										//postParam.put(JsonValues.description, Descriptions.PUSH_ATTENDANCE);
										//postParam.put("route", URLs.PUSH_ATTENDANCE);
										//postParam.put(JsonValues.teacher_name,teacher_name);
										//postParam.put(JsonValues.class_name,attendance.getClassName());
										postParam.put(JsonValues.subject_id,attendance.getSubject_id()+"");//id
										postParam.put(JsonValues.module_id, attendance.getModule_id()+"");//id
										postParam.put(JsonValues.timeslot_id, attendance.getSlot().getSlot_id());
										postParam.put(JsonValues.content_type,
												"application/x-www-form-urlencoded");
										String attendance_type="0";
										if(attendance.getType().equalsIgnoreCase("lab"))
											attendance_type="1";
										postParam.put(JsonValues.attendance_type,attendance_type);
										postParam.put(JsonValues.date, attendance.getDate());
										postParam.put(JsonValues.timestamp, attendance.getTimestamp());
										JSONObject postObject = new JSONObject(postParam);
										JSONArray presentJSONArray=new JSONArray();
										JSONArray absentJSONArray=new JSONArray();
										while (cur.moveToNext()) {
											Integer student_id=cur.getInt(3);
											Cursor cur1=db.rawQuery("", null);
											String present = cur.getString(2);// gets class_id
											if(present.equals("true"))
												presentJSONArray.put(student_id);
											else
												absentJSONArray.put(student_id);
										}
										try {
											postObject.put(JsonValues.present_array, presentJSONArray);
											postObject.put(JsonValues.absent_array, absentJSONArray);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										Log.e("Push Attendance JSON Req:", postObject.toString());
										String url = URLs.BASE_URL + URLs.PUSH_ATTENDANCE;
										RequestQueue requestQueue=Volley.newRequestQueue(context);
										JsonObjectRequest jsonRequest=new JsonObjectRequest(Request.Method.POST,url,postObject,
												new Response.Listener<JSONObject>(){

													@Override
													public void onResponse(
															final JSONObject response,
															int type) {
														// TODO Auto-generated method stub
														new Thread(new Runnable(){
															@Override
															public void run() {
																// TODO Auto-generated method stub
																//Check for Successful pushing of attendance
																//and delete records from database
																try {
																	if (response
																			.getBoolean("success")) {
																		((Activity)context).runOnUiThread(new Runnable() {
																			
																			@Override
																			public void run() {
																				// TODO Auto-generated method stub
																				MessageHelper m=new MessageHelper(context);
																				m.displayShort("Successful Submission");
																			}
																		});
																		DataHelper helper = new DataHelper(
																				context);
																		SQLiteDatabase db = helper
																				.getWritableDatabase();
																		db.execSQL("Delete from attendance where pendingattendance_id="
																				+ attendance
																						.getAttendance_id()
																				+ ";");
																		db.execSQL("Delete from pendingattendance where id="
																				+ attendance
																						.getAttendance_id()
																				+ ";");
																	}
																} catch (SQLException e) {
																	// TODO
																	// Auto-generated
																	// catch block
																	e.printStackTrace();
																} catch (JSONException e) {
																	// TODO
																	// Auto-generated
																	// catch block
																	e.printStackTrace();
																}Log.e("Push Attendance JSON Reply:", response.toString());
																//((Activity)context).finish();
																//list.remove(position);
																//notifyDataSetChanged();
																Intent i = ((Activity) context)
																		.getIntent();
																((Activity) context)
																		.finish();
																context
																		.startActivity(i);
															}
														}).start();
													}

													@Override
													public void onResponseHeaders(
															Map<String, String> headers,
															int type) {
														// TODO Auto-generated method stub
														
														
													}
											
										},new Response.ErrorListener(){

											@Override
											public void onErrorResponse(
													VolleyError error, int type) {
												// TODO Auto-generated method stub
												Log.e("PushAttendanceError", error.toString());
												Toast.makeText(context,
														"Unable to submit attendance  ",
														Toast.LENGTH_LONG).show();
											}
										},0);
										jsonRequest
										.setRetryPolicy((RetryPolicy) new DefaultRetryPolicy(
												10000000,
												DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
												DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
										requestQueue.add(jsonRequest);
									}
								}
								
							}).start();
						}
					});
					builder2.show();
					
					
				}
				else {
					Toast.makeText(context, "No Internet Connection",
							Toast.LENGTH_LONG).show();
					}
			}
		});
		
		return row;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	static class Holder {
		private TextView className;
		private TextView dateSlot;
		private TextView subjectTopic;
		private Button push;
		private TextView attendanceType;
	}
}
