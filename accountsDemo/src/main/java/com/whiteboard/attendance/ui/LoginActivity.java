package com.whiteboard.attendance.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.accountsdemo.R;
import com.whiteboard.attendance.helper.DataHelper;
import com.whiteboard.attendance.helper.MenuItems;
import com.whiteboard.attendance.helper.MessageHelper;
import com.whiteboard.attendance.util.Classs;
import com.whiteboard.attendance.util.Descriptions;
import com.whiteboard.attendance.util.JsonValues;
import com.whiteboard.attendance.util.Student;
import com.whiteboard.attendance.util.Subject;
import com.whiteboard.attendance.util.Teacher;
import com.whiteboard.attendance.util.TimeSlot;
import com.whiteboard.attendance.util.Topic;
import com.whiteboard.attendance.util.URLs;
import com.whiteboard.attendance.util.Values;

public class LoginActivity extends AppCompatActivity {

	String accountType;
	Account accounts[];
	Context c;
	Button login, login1;
	AccountManager manager;
	EditText password;
	RequestQueue requestQueue;
	EditText username;
	MessageHelper m;
	ArrayList<TimeSlot> slotList = new ArrayList<TimeSlot>();
	ArrayList<Integer> classIDs = new ArrayList<Integer>();
	ArrayList<Integer> subjectIDs = new ArrayList<Integer>();
	String access_token = "";
	ProgressDialog d;

	public LoginActivity() {
		accountType = "com.whiteboard";
	}

	public void initializer() {
		SharedPreferences sharedpreferences = getSharedPreferences(
				Values.USER_INFO, 0);
		Values.setUsername(sharedpreferences.getString(Values.USER_NAME, ""));
		Values.setAccess_token(sharedpreferences.getString(Values.ACCESS_TOKEN,
				""));
	}

	protected void onCreate(Bundle bundle) {
		c = LoginActivity.this;
		m = new MessageHelper(c);
		super.onCreate(bundle);
		setContentView(R.layout.activity_main);
		password = (EditText) findViewById(R.id.password);
		username = (EditText) findViewById(R.id.username);
		login = (Button) findViewById(R.id.login);
		manager = AccountManager.get(c);
		accounts = manager.getAccountsByType(accountType);
		initializer();
		if (accounts.length > 0) {
			// For selection of a login account from multiple accounts
			// AlertDialog.Builder builder =new AlertDialog.Builder(c);
			// builder.setTitle("Select Login Account");
			// builder.setSingleChoiceItems(new CharSequence[]{"abc"}, -1, new
			// DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int item) {
			// TODO Auto-generated method stub

			// }
			// });
			Intent i = new Intent(LoginActivity.this,
					PendingAttendanceActivity.class);
			startActivity(i);
			finish();
		}
		class ResponseTask extends AsyncTask<Response, Void, Void> {

			@Override
			protected Void doInBackground(Response... params) {
				// TODO Auto-generated method stub
				return null;
			}

		}
		login.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final String user = username.getText().toString();
				final String pwd = password.getText().toString();
				final Account account = new Account(user, accountType);
				ConnectivityManager cm = (ConnectivityManager) c
						.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo netInfo = cm.getActiveNetworkInfo();

				if (netInfo != null && netInfo.isConnectedOrConnecting()) {// checking
																			// if
																			// connectivity
																			// is
																			// available
					if (user.equals("") || password.equals("")) {
						m.displayShort("Incorrect username and/or password");
					} else {
						final String url = URLs.BASE_URL + URLs.LOGIN_REQUEST;
						Map<String, String> postParam = new HashMap<String, String>();
						// details of JSON to be sent
						// postParam.put("title", "Faculty Login");
						postParam.put(JsonValues.grant_type, "password");
						postParam.put(JsonValues.description,
								Descriptions.LOGIN_REQUEST);
						// postParam.put("route", URLs.LOGIN_REQUEST);
						postParam.put(JsonValues.client_id, Values.client_id);
						postParam.put(JsonValues.client_secret,
								Values.client_secret);
						postParam.put(JsonValues.username, user);
						postParam.put(JsonValues.password, pwd);
						postParam.put(JsonValues.content_type,
								"application/x-www-form-urlencoded");
						// JSON Object consisting of all the information of user
						final JSONObject postObject = new JSONObject(postParam);
						/*
						 * JsonObject json=new JsonObject();
						 * json.addProperty("cliend_id",
						 * "f3d259ddd3ed8ff3843839b");
						 * json.addProperty("client_secret",
						 * "4c7f6f8fa93d59c45502c0ae8c4a95b");
						 * json.addProperty("grant_type", "password");
						 * json.addProperty("username", user);
						 * json.addProperty("password", pwd);
						 */
						Log.e("Login JSON Req:", postObject.toString());
												new Thread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								LoginActivity.this.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										d = new ProgressDialog(c);
										d.setProgressStyle(0);
										d.setTitle("Authenticating");
										d.show();
										d.setCancelable(false);
									}
								});
								requestQueue = Volley.newRequestQueue(c);
								// JSONRequest to post JSON object consisting of user
								// info
								JsonObjectRequest jsonRequest = new JsonObjectRequest(
										Request.Method.POST, url, postObject,
										new Response.Listener<JSONObject>() {
											@Override
											public void onResponse(
													final JSONObject response, int type) {
												// TODO Auto-generated method stub
												// On response received from server
												new Thread(new Runnable() {
													@Override
													public void run() {
														// TODO Auto-generated method
														// stub
														try {
															Log.e("Login JSON Req:",
																	response.toString());
															// store data JSON Object
															JSONObject dataObject = response
																	.getJSONObject(JsonValues.data);
															// store token JSON Object
															// from dataObject
															JSONObject tokenObject = dataObject
																	.getJSONObject(JsonValues.token);
															// get access_token,
															// token_type, expires in
															// from tokenObject
															access_token = (String) tokenObject
																	.get(JsonValues.access_token);
															String token_type = (String) tokenObject
																	.get(JsonValues.token_type);
															String expires_in = ""
																	+ tokenObject
																			.get(JsonValues.expires_in);
															// store user JSON Object
															// from dataObject
															JSONObject userObject = dataObject
																	.getJSONObject(JsonValues.user);
															// get name,email from
															// userObject
															String name = (String) userObject
																	.get(JsonValues.name);
															String email = (String) userObject
																	.get(JsonValues.email);
															// store timeslots array
															JSONArray timeSlots = dataObject
																	.getJSONArray(JsonValues.slot_array);
															int l = 0;
															while (l < timeSlots
																	.length()) {
																JSONObject jo = (JSONObject) timeSlots
																		.get(l);
																TimeSlot t = new TimeSlot();
																String slot_id = jo
																		.getString(JsonValues.slot_id);
																String start_time = jo
																		.getString(JsonValues.start_time);
																String end_time = jo
																		.getString(JsonValues.end_time);
																// //t.setSlot_name(slot_name);
																t.setSlot_id(slot_id);
																t.setEnd_time(end_time);
																t.setStart_time(start_time);
																slotList.add(t);
																l++;
															}
															// String deptname =
															// (String) response
															// .get(JsonValues.department_name);//
															// Dept
															// name
															// Create Teacher Object and
															// store name and department
															Teacher teacher = Values
																	.getTeacher();
															Classs classs;
															Subject subject;
															Topic topic;
															SharedPreferences.Editor editor = getSharedPreferences(
																	Values.USER_INFO, 0)
																	.edit();
															editor.putString(
																	Values.TEACHER_NAME,
																	name);
															teacher.setName(name);
															// teacher.setDepartment(deptname);
															JSONArray class_array = dataObject
																	.getJSONArray(JsonValues.class_array);
															l = 0;
															// for each element in
															// JSONArray details
															ArrayList<Classs> classes = new ArrayList<Classs>();
															while (l < class_array
																	.length()) {
																JSONObject jo = (JSONObject) class_array
																		.get(l);
																// retrieve classname
																classs = new Classs();
																String classname = (String) jo
																		.get(JsonValues.class_name);// ClassName
																Log.e("ClassNAme", classname);
																String classCapacity = (String) jo
																		.get(JsonValues.class_capacity);// ClassCapacity
																String department = jo
																		.getString(JsonValues.department_name);
																String id = jo
																		.getString(JsonValues.id);
																classs.setClassCapacity(Integer
																		.parseInt(classCapacity));
																classs.setName(classname);
																classs.setDepartment(department);
																classs.setId(id);
																// Log.e("CLASS_DEPT",
																// department);
																ArrayList<Student> rollNo_List = new ArrayList<Student>();
																JSONArray student_array = jo
																		.getJSONArray(JsonValues.rollNo_array);
																Student s;
																int p = 0;
																while (p < student_array
																		.length()) {
																	s = new Student();
																	JSONObject rollNoObject = student_array
																			.getJSONObject(p);
																	s.setRoll_no(rollNoObject
																			.getString("roll_no"));
																	s.setId(rollNoObject
																			.getString("user_id"));
																	rollNo_List.add(s);
																	p++;
																}
																classs.setRollNoList(rollNo_List);
																JSONArray subject_array = jo
																		.getJSONArray(JsonValues.subject_array);
																// for each element in
																// JSONArray subjects
																// for given class
																ArrayList<Subject> subjects = new ArrayList<Subject>();
																int l1 = 0;
																while (l1 < subject_array
																		.length()) {
																	JSONObject jo1 = (JSONObject) subject_array
																			.get(l1);
																	subject = new Subject();
																	String subjectname = (String) jo1
																			.get(JsonValues.subject_name);
																	Log.e("subjectNAme", subjectname);
																	
																	String isElective = jo1
																			.getString(JsonValues.subject_elective);
																	id = jo1.getString(JsonValues.id);
																	if (isElective
																			.equals("1")) {
																		rollNo_List = new ArrayList<Student>();
																		subject.setIsElective(true);
																		JSONArray rollNo_array = jo1
																				.getJSONArray(JsonValues.rollNo_array);
																		p = 0;
																		while (p < rollNo_array
																				.length()) {
																			s = new Student();
																			JSONObject rollNoObject = rollNo_array
																					.getJSONObject(p);
																			s.setRoll_no(rollNoObject
																					.getString("roll_no"));
																			s.setId(rollNoObject
																					.getString("id"));
																			rollNo_List
																					.add(s);
																			p++;
																		}
																		subject.setRollNoList(rollNo_List);
																	}
																	subject.setId(id);
																	subject.setName(subjectname);
																	JSONArray topic_array = jo1
																			.getJSONArray(JsonValues.topic_array);
																	// for each element
																	// in JSONArray
																	// topics for given
																	// subject
																	ArrayList<Topic> topics = new ArrayList<Topic>();
																	int l2 = 0;
																	while (l2 < topic_array
																			.length()) {
																		topic = new Topic();
																		JSONObject obj1 = topic_array
																				.getJSONObject(l2);
																		String topicname = obj1
																				.getString(JsonValues.topic_name);
																		id = obj1
																				.getString(JsonValues.id);
																		topic.setName(topicname);
																		topic.setId(id);
																		topics.add(topic);
																		l2++;
																	}
																	subject.setTopics(topics);
																	// topics.clear();
																	subjects.add(subject);
																	l1++;
																}
																classs.setSubjects(subjects);
																// subjects.clear();
																classes.add(classs);
																l++;
															}
															teacher.setClasses(classes);
															// classes.clear();
															//Log.e("classes_length",teacher.getClasses().size()+"");
															storeInDB(teacher);
														} catch (JSONException e1) {
															// TODO Auto-generated catch
															// block
															e1.printStackTrace();
														}
														SharedPreferences.Editor editor = getSharedPreferences(
																Values.USER_INFO, 0)
																.edit();
														Log.e("A", access_token);
														editor.putString(
																Values.ACCESS_TOKEN,
																access_token);
														editor.putString(
																Values.USER_NAME, user);
														editor.commit();
														// SharedPreferences
														// sp=getSharedPreferences(Values.USER_INFO,
														// 0);
														// Log.e("ACCESS_TOKEN",
														// sp.getString(Values.ACCESS_TOKEN,
														// ""));
														if (manager
																.addAccountExplicitly(
																		account, pwd,
																		null)) {
															LoginActivity.this
																	.runOnUiThread(new Runnable() {

																		@Override
																		public void run() {
																			// TODO
																			// Auto-generated
																			// method
																			// stub
																			// Toast.makeText(c,
																			// "Login Successful",
																			// Toast.LENGTH_SHORT)
																			// .show();
																			d.dismiss();
																			m.displayShort("LoginSuccessful");
																		}
																	});
															Intent i = new Intent(
																	LoginActivity.this,
																	PendingAttendanceActivity.class);
															startActivity(i);
															finish();
														} else {
															Toast.makeText(
																	c,
																	"Account creation failed",
																	Toast.LENGTH_SHORT)
																	.show();
															Log.e("TAG",
																	"Account creation failed.");
														}
														
													}
												}).start();
											}

											@Override
											public void onResponseHeaders(
													Map<String, String> headers,
													int type) {
												// TODO Auto-generated method stub

											}
										}, new Response.ErrorListener() {

											@Override
											public void onErrorResponse(
													VolleyError error, int type) {
												// TODO Auto-generated method stub
												Log.e("LoginError", error.toString());
												// Error response implies login
												// credentials are invalid
												Toast.makeText(c,
														"Invalid Login Credentials",
														Toast.LENGTH_LONG).show();
												d.dismiss();
											}
										}, 0);
								jsonRequest
										.setRetryPolicy((RetryPolicy) new DefaultRetryPolicy(
												10000000,
												DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
												DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
								requestQueue.add(jsonRequest);
							}
						}).start();
						
					}
				} else {
					Toast.makeText(c, "No Internet Connection",
							Toast.LENGTH_LONG).show();

				}
			}
		});
	}

	public void storeInDB(Teacher teacher) {
		DataHelper helper = new DataHelper(c);
		SQLiteDatabase db = helper.getWritableDatabase();
		Cursor cur;
		ArrayList<Classs> classes;
		ArrayList<Subject> subjects;
		ArrayList<Topic> topics;
		ArrayList<Student> students;
		ContentValues contentValues = new ContentValues();
		contentValues.put("type", "Lab");
		db.insert("attendancetype", null, contentValues);
		contentValues.clear();
		contentValues.put("type", "Class");
		db.insert("attendancetype", null, contentValues);
		contentValues.clear();
		// put in content values for teacher details
		contentValues.put("name", teacher.getName());
		// contentValues.put("dept", teacher.getDepartment());
		// store content values in teacher table
		db.insert("teacher", null, contentValues);
		contentValues.clear();
		int l = 0;
		TimeSlot t;
		while (l < slotList.size()) {
			db = helper.getWritableDatabase();
			t = slotList.get(l);
			contentValues.put("start_time", t.getStart_time());
			contentValues.put("end_time", t.getEnd_time());
			contentValues.put("id", t.getSlot_id());
			contentValues
					.put("name", t.getStart_time() + "-" + t.getEnd_time());
			db.insert("slots", null, contentValues);
			contentValues.clear();
			l++;
		}
		// get all classes details
		classes = teacher.getClasses();
		Log.e("classes", "msg"+classes.size());//classes not getting loaded
		Iterator<Classs> i = classes.iterator();
		while (i.hasNext()) {
			Classs c = i.next();
			db = helper.getWritableDatabase();
			String a = c.getId();
			Integer id = Integer.valueOf(a);
			if (classIDs.contains(id)) {
				Log.e("classes", "msg");
			} else {
				classIDs.add(id);
				Log.e("classes", c.getName());
				contentValues.put("id", c.getId());
				contentValues.put("name", c.getName());
				contentValues.put("capacity", c.getClassCapacity());
				contentValues.put("dept", c.getDepartment());
				// Log.e("CLASS", a + " " + c.getName() + " " +
				// c.getClassCapacity()
				// + " " + c.getDepartment());
				db.insert("classes", null, contentValues);
				contentValues.clear();
				students = c.getRollNoList();
				Iterator<Student> i4 = students.iterator();
				while (i4.hasNext()) {
					Student s = i4.next();
					contentValues.put("class_id", a);
					contentValues.put("roll_no", s.getRoll_no());
					contentValues.put("student_id", s.getId());
					db.insert("class_rollno", null, contentValues);
					contentValues.clear();
				}
				subjects = c.getSubjects();
				Iterator<Subject> i1 = subjects.iterator();
				while (i1.hasNext()) {
					Subject s = i1.next();
					db = helper.getWritableDatabase();
					String a1 = s.getId();
					contentValues.put("id", s.getId());
					contentValues.put("name", s.getName());
					contentValues.put("class_id", a);
					Log.e("ClassName", c.getName());
					Log.e("SubjectName", s.getName());
					if (s.getIsElective()) {
						contentValues.put("is_elective", "1");
						contentValues.put("classCapacity", s.getRollNoList()
								.size());
						// Log.e("SUBJECT", a1 + " " +
						// s.getRollNoList().size());
						db.insert("subjects", null, contentValues);
						contentValues.clear();
						ArrayList<Student> rollNoList = s.getRollNoList();
						Iterator<Student> iterator = rollNoList.iterator();
						while (iterator.hasNext()) {
							Student s1 = iterator.next();
							db = helper.getWritableDatabase();
							contentValues.put("subject_id", a1);
							contentValues.put("roll_no", s1.roll_no);
							contentValues.put("student_id", s1.id);
							db.insert("subject_rollno", null, contentValues);
							contentValues.clear();
						}

					} else {
						contentValues.put("is_elective", "0");
						contentValues.put("classCapacity", "0");
						db.insert("subjects", null, contentValues);
						contentValues.clear();
					}
					topics = s.getTopics();
					Iterator<Topic> i2 = topics.iterator();
					while (i2.hasNext()) {
						Topic topic = i2.next();
						contentValues.put("id", topic.getId());
						contentValues.put("name", topic.getName());
						contentValues.put("subject_id", a1);
						db.insert("topics", null, contentValues);
						contentValues.clear();
					}

				}
			}
		}
		//db.close();


		/*
		 * {// Checking db = helper.getReadableDatabase(); Cursor cur =
		 * db.rawQuery("Select * from classes", null); // String ids="";
		 * Log.e("classes", cur.getCount() + ""); if (cur != null) { while
		 * (cur.moveToNext()) { Log.e("classes", cur.getString(0) + "  " +
		 * cur.getString(1) + "  " + cur.getString(2) + "  " +
		 * cur.getString(3)); } } cur = db.rawQuery("Select * from subjects",
		 * null); // String ids=""; if (cur != null) { while (cur.moveToNext())
		 * { Log.e("subjects", cur.getString(0) + "  " + cur.getString(1) + "  "
		 * + cur.getString(2) + "  " + cur.getString(3) + "  " +
		 * cur.getString(4)); } } cur =
		 * db.rawQuery("Select * from subject_rollno", null); // String ids="";
		 * if (cur != null) { while (cur.moveToNext()) { Log.e("subject_rollno",
		 * cur.getString(0) + "  " + cur.getString(1) + "  " +
		 * cur.getString(2)); } } cur = db.rawQuery("Select * from topics",
		 * null); // String ids=""; if (cur != null) { while (cur.moveToNext())
		 * { Log.e("topics", cur.getString(0) + "  " + cur.getString(1) + "  " +
		 * cur.getString(2)); } } cur =
		 * db.rawQuery("Select * from pendingattendance", null); // String
		 * ids=""; if (cur != null) { while (cur.moveToNext()) {
		 * Log.e("pendingattendance", cur.getString(0) + "  " + cur.getString(1)
		 * + "  " + cur.getString(2) + "  " + cur.getString(3) + " " +
		 * cur.getString(4) + "  " + cur.getString(5) + "  " + cur.getString(6)
		 * + "  " + cur.getString(7)); } } cur =
		 * db.rawQuery("Select * from attendance", null); // String ids=""; if
		 * (cur != null) { while (cur.moveToNext()) { Log.e("attendance",
		 * cur.getString(0) + "  " + cur.getString(1) + "  " +
		 * cur.getString(2)); } } cur = db.rawQuery("Select * from slots",
		 * null); // String ids=""; if (cur != null) { while (cur.moveToNext())
		 * { Log.e("slots", cur.getString(0) + "  " + cur.getString(1) + "  " +
		 * cur.getString(2)); } } cur =
		 * db.rawQuery("Select * from attendancetype", null); // String ids="";
		 * if (cur != null) { while (cur.moveToNext()) { Log.e("attendancetype",
		 * cur.getString(0) + "  " + cur.getString(1)); } } cur =
		 * db.rawQuery("Select * from teacher", null); // String ids=""; if (cur
		 * != null) { while (cur.moveToNext()) { Log.e("teacher",
		 * cur.getString(0) + "  " + cur.getString(1)); } } }
		 */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		MenuItems items = new MenuItems(c);
		items.process(id);
		return super.onOptionsItemSelected(item);
	}
}
