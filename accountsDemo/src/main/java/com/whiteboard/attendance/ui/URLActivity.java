package com.whiteboard.attendance.ui;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.accountsdemo.R;
import com.whiteboard.attendance.helper.MenuItems;
import com.whiteboard.attendance.util.URLs;
import com.whiteboard.attendance.util.Values;

public class URLActivity extends AppCompatActivity{
	Context c;
	EditText url;
	Button submit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		c=this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_url);
		url=(EditText)findViewById(R.id.url);
		submit=(Button)findViewById(R.id.submit);
		url.setText(URLs.BASE_URL);
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String value=url.getText().toString();
				if(value.contains(".")){
					SharedPreferences.Editor editor = getSharedPreferences(
							Values.USER_INFO, 0).edit();
					editor.putString("URL",
							value);
					URLs.BASE_URL=value;
					Toast.makeText(c, "URL:"+value, Toast.LENGTH_SHORT).show();
					Log.e("URL VALUE",value);
				}
				else{
					Toast.makeText(c, "Insert appropriate URL", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		MenuItems items=new MenuItems(c);
		items.process(id);
		return super.onOptionsItemSelected(item);
	}
}
